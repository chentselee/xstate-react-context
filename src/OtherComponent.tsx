import { useSelector } from "@xstate/react";
import { counterContext } from "./counter.context";

const OtherComponent = () => {
  const [, , useCounterService] = counterContext;
  const counterService = useCounterService();
  const randomContext = useSelector(
    counterService,
    (state) => state.context.randomContext
  );
  console.log("other component rendered");
  return (
    <div>
      <div>other component</div>
      <div>other context: {randomContext}</div>
    </div>
  );
};

export default OtherComponent;
