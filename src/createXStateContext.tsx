import { createContext, useContext } from "react";
import { ActorRefFrom, StateMachine } from "xstate";
import { useActor as useXStateActor, useInterpret } from "@xstate/react";

/**
 * A wrapper for creating type-safe XState Provider and hooks (inspired by https://github.com/mattpocock/xstate-next-boilerplate/blob/main/src/lib/createXStateContext.ts)
 * @returns a tuple of [Provider, useActor, useService]
 */
export const createXStateContext = <
  TMachine extends StateMachine<any, any, any>
>(
  machine: TMachine
) => {
  const context = createContext<ActorRefFrom<typeof machine>>(
    null as unknown as ActorRefFrom<typeof machine>
  );

  const Provider: React.FC = ({ children }) => {
    return (
      <context.Provider
        value={useInterpret(machine) as unknown as ActorRefFrom<typeof machine>}
      >
        {children}
      </context.Provider>
    );
  };

  const useService = () => {
    const service = useContext(context);
    if (!service) {
      throw new Error(
        `use${machine.id}Service must be used inside ${machine.id}Provider`
      );
    }
    return service;
  };

  const useActor = () => {
    const service = useService();
    return useXStateActor(service);
  };

  return [Provider, useActor, useService] as const;
};
