import { createMachine } from "xstate";

type AuthMachineContext = undefined;

type LogInEvent = { type: "LOG_IN"; username: string; password: string };
type LogOutEvent = { type: "LOG_OUT" };

type AuthMachineEvent = LogInEvent | LogOutEvent;

type AuthMachineState =
  | { value: "unauthorized"; context: AuthMachineContext }
  | { value: "authorized"; context: AuthMachineContext };

export const authMachine = createMachine<
  AuthMachineContext,
  AuthMachineEvent,
  AuthMachineState
>(
  {
    initial: "unauthorized",
    states: {
      unauthorized: {
        on: {
          LOG_IN: { cond: "correct", target: "authorized" },
        },
      },
      authorized: {
        on: {
          LOG_OUT: "unauthorized",
        },
      },
    },
  },
  {
    guards: {
      correct: (_, event) => {
        return (
          (event as LogInEvent).username === "123" &&
          (event as LogInEvent).password === "456"
        );
      },
    },
  }
);
