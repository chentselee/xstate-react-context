import { createXStateContext } from "./createXStateContext";
import { authMachine } from "./auth.machine";

export const authContext = createXStateContext(authMachine);
