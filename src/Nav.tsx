import { useState } from "react";
import { authContext } from "./auth.context";

const Nav = () => {
  const [, useActor] = authContext;
  const [state, send] = useActor();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  return (
    <nav>
      {state.matches("authorized") && (
        <button onClick={() => send("LOG_OUT")}>log out</button>
      )}
      {state.matches("unauthorized") && (
        <div>
          <label htmlFor="username">
            Username
            <input
              type="text"
              name="username"
              id="username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </label>
          <label htmlFor="password">
            Password
            <input
              type="password"
              name="password"
              id="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </label>
          <button onClick={() => send({ type: "LOG_IN", username, password })}>
            log in
          </button>
        </div>
      )}
    </nav>
  );
};

export default Nav;
