import { createMachine, assign, interpret } from "xstate";

interface CounterMachineContext {
  count: number;
  randomContext: string;
}

type ToggleEvent = { type: "TOGGLE" };
type IncrementEvent = { type: "INCREMENT"; by: number };
type DecrementEvent = { type: "DECREMENT"; by: number };
type ResetEvent = { type: "RESET" };

type CounterMachineEvent =
  | ToggleEvent
  | IncrementEvent
  | DecrementEvent
  | ResetEvent;

type OnState = {
  value: "on";
  context: CounterMachineContext;
};
type OffState = {
  value: "off";
  context: CounterMachineContext & { count: undefined };
};

type CounterMachineState = OnState | OffState;

const initialContext: CounterMachineContext = {
  count: 0,
  randomContext: "random context",
};

export const counterMachine = createMachine<
  CounterMachineContext,
  CounterMachineEvent,
  CounterMachineState
>(
  {
    initial: "on",
    context: initialContext,
    states: {
      on: {
        on: {
          TOGGLE: "off",
          INCREMENT: {
            actions: "increment",
          },
          DECREMENT: {
            actions: "decrement",
          },
          RESET: {
            actions: "reset",
          },
        },
      },
      off: {
        on: {
          TOGGLE: "on",
        },
      },
    },
  },
  {
    actions: {
      increment: assign({
        count: (context, event) => {
          if (event.type !== "INCREMENT") return initialContext.count;
          return context.count + event.by;
        },
      }),
      decrement: assign({
        count: (context, event) => {
          if (event.type !== "DECREMENT") return initialContext.count;
          return context.count - event.by;
        },
      }),
      reset: assign({ count: (_) => 0 }),
    },
  }
);

export const counterService = interpret(counterMachine).start();
