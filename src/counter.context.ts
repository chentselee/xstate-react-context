import { createXStateContext } from "./createXStateContext";
import { counterMachine } from "./counter.machine";

export const counterContext = createXStateContext(counterMachine);
