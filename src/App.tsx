import Counter from "./Counter";
import { counterContext } from "./counter.context";
import { authContext } from "./auth.context";
import OtherComponent from "./OtherComponent";
import Nav from "./Nav";

function App() {
  const [CounterProvider] = counterContext;
  const [AuthProvider] = authContext;
  return (
    <AuthProvider>
      <Nav />
      <CounterProvider>
        <Counter />
        <OtherComponent />
      </CounterProvider>
    </AuthProvider>
  );
}

export default App;
