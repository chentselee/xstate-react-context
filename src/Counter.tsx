import { counterContext } from "./counter.context";

const Counter = () => {
  const [, useCounterActor] = counterContext;
  const [state, send] = useCounterActor();
  console.log("counter rendered");
  return (
    <div>
      <h1>count: {state.context.count}</h1>
      <button onClick={() => send("TOGGLE")}>
        {state.matches("on") ? "off" : "on"}
      </button>
      {state.matches("on") && (
        <div>
          <button onClick={() => send({ type: "INCREMENT", by: 2 })}>
            increment
          </button>
          <button onClick={() => send({ type: "RESET" })}>reset</button>
          <button onClick={() => send({ type: "DECREMENT", by: 2 })}>
            decrement
          </button>
        </div>
      )}
    </div>
  );
};

export default Counter;
